const gradeController = require('../controllers/grade.controller');

const router = require('express').Router();

router.post('/',
    gradeController.postGrade);
router.get('/',
    gradeController.getAllGrade);


module.exports = router;