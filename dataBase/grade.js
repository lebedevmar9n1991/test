const { Schema, model } = require('mongoose');

const gradeSchema = new Schema({
    userName: {
        type: String,
        required: true,
        trim: true,
    },
    grade: {
        type: String,
        required: true
    },
    comment: String
}, { timestamps: true });

module.exports = model('grades', gradeSchema);