# README

## Prerequisites

Before running the application, you need the following installed:


Node.js

MongoDB


## Run application

    To run the application, follow these steps:



- Clone this repository and navigate to it.



-  Install the dependencies with the following command:


        npm install




-  Start the server with the following command:


        npm start\n\n           
     This will start the server on port 8080.




- To access the API documentation, open http://localhost:8080/api-docs/ in your web browser.