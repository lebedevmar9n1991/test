const { Grade } = require("../dataBase");

module.exports = {
    createOneGrade: (grade) => {
        return Grade.create(grade);
    },
    findAllGrades: (params={}) => {
        return Grade.find(params);
    }
};