const gradeService = require("../service/grade.service");

module.exports = {
    postGrade: async (req, res, next) => {
        try {
            // const { userName, grade, comment } = req.body;

            const newGrade = await gradeService.createOneGrade(req.body);

            res.status(200).json(newGrade);
        } catch (error) {
            next(error);
        }
    },
    getAllGrade: async (req, res, next) => {
        try {
            const allGrades = await gradeService.findAllGrades();

            res.status(201).json(allGrades);
        } catch (error) {
            next(error);
        }
    }
};